"use strict";

window.onload = function () {

    let chart = new CanvasJS.Chart("chartContainer", {
        theme: "light1", // "light2", "dark1", "dark2"
        animationEnabled: false, // change to true		
        title:{
            text: "Basic Column Chart"
        },
        data: [
        {
            // Change type to "bar", "area", "spline", "pie",etc.
            type: "column",
            dataPoints: [
                { label: "enero",  y: 10  },
                { label: "febrero", y: 15  },
                { label: "marzo", y: 25  },
                { label: "abril",  y: 30  },
                { label: "mayo",  y: 28  },
                { label: "junio",  y: 28  },
                { label: "julio",  y: 28  },
                { label: "agosto",  y: 28  },
                { label: "septiembre",  y: 28  },
                { label: "octubre",  y: 28  },
                { label: "noviembre",  y: 28  },
                { label: "diciembre",  y: 28  }
            ]
        }
        ]
    });

    chart.render();
}
