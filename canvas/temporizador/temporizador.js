"use strict";

let intervalID = null;
let incremento = 0;

window.addEventListener('load', () => {

    document.getElementById('palante').addEventListener('click', iniciarTemporizadorPalante);
    document.getElementById('patras').addEventListener('click', iniciarTemporizadorPatras);
    document.getElementById('parar').addEventListener('click', detenerTemporizador);

});

function iniciarTemporizadorPalante() {
    incremento = 1;

    if(intervalID == null) {
        intervalID = setInterval(actualizarTemporizador, 1000);
    }
}

function iniciarTemporizadorPatras() {
    incremento = -1;

    if(intervalID == null) {
        intervalID = setInterval(actualizarTemporizador, 1000);
    }
}

function detenerTemporizador() {
    incremento = 0;
    clearInterval(intervalID);
    intervalID = null;
}

function actualizarTemporizador() {
    let temporizador = document.getElementById('temporizador');
    temporizador.value = Number(temporizador.value) + incremento;
}